<a href="#"><img title="Open Source" src="https://img.shields.io/badge/Open%20Source-%E2%9D%A4-green?style=for-the-badge"></a>
</p>
<a href="#"><img title="<<GNU GPLv3 "  src="https://img.shields.io/badge/license-%20%20GNU%20GPLv3%20-green?style=for-the-badge"></a>

# ¿Cómo funciona?
<p>La herramienta genera una página HTTPS maliciosa usando los métodos Serveo o Ngrok Port Forwarding, y un código javascript para cam peticiones usando MediaDevices.getUserMedia. </p>

<p>El método MediaDevices.getUserMedia() solicita al usuario permiso para utilizar una entrada multimedia que produce un MediaStream con pistas que contienen los tipos de medios solicitados. Ese flujo puede incluir, por ejemplo, una pista de vídeo (producida por una fuente de vídeo física o virtual, como una cámara, un dispositivo de grabación de vídeo, un servicio de compartición de pantalla, etc.), una pista de audio (de forma similar, producida por una fuente de audio física o virtual, como un micrófono, un convertidor A/D, etc.) y, posiblemente, otros tipos de pistas. </p>

[Más información sobre MediaDEvices.getUserMedia() aquí](https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia)
<p>Para convencer al objetivo de que conceda permisos para acceder a la cámara, la página utiliza un código javascript realizado por https://github.com/wybiral que convierte el favicon en un stream de la cámara.</p> <p>

## Installing:

```
git https://gitlab.com/x3r0x0/hacking-cam
cd hacking-cam
bash hacking-cam
```

